﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;




public class PushString
{
    public string Text = "";

    public PushString(string text)
    {
        Text = text;
    }

    public PushString(byte[] data)
    {
        if (data != null)
            Text = Encoding.UTF8.GetString(data, 0, data.Length);
    }



    public bool ContainKey(string key)
    {
        if (Text.Contains("\u0010" + key + "\u0012"))
            return true;
        else
            return false;
    }
    public bool ContainTag(string tag)
    {
        if (Text.Contains("\u0010" + tag + "\u0011"))
            return true;
        else
            return false;
    }

    public string GetString(string key)
    {
        if (!Text.Contains("\u0010" + key + "\u0012"))
            return null;
        int bracketCount = 0;
        int index = Text.IndexOf("\u0010" + key + "\u0012");
        if (index >= 0)
        {
            string temp = Text.Substring(index + key.Length + 2);
            for (int i = 0; i < temp.Length; i++)
            {
                if (temp[i] == '\u0010')
                {
                    bracketCount++;
                }
                else
                    if (temp[i] == '\u0011')
                    bracketCount--;
                if (bracketCount < 0)
                    return temp.Substring(0, i);
            }
            return temp;
        }
        return null;
    }

    public string[] GetStrings(string key)
    {
        string temp = GetString(key);
        if (temp == null)
            return null;
        var result = new List<string>();
        int openBracketCount = 0;
        int recordedPosition = 0;
        for (int i = 0; i < temp.Length; i++)
        {
            if (i == temp.Length - 1)
            {
                result.Add(temp.Substring(recordedPosition, i + 1 - recordedPosition));
            }
            else
                if (temp[i] == '\u0010')
                openBracketCount++;
            else
                    if (temp[i] == '\u0011')
                openBracketCount--;
            else
                        if (temp[i] == '\u0012' && openBracketCount == 0)
            {
                result.Add(temp.Substring(recordedPosition, i - recordedPosition));
                recordedPosition = i + 1;
            }
        }
        return result.ToArray();
    }

    public static string SetTag(string tag)
    {
        return "\u0010" + tag + "\u0011";
    }

    public static string SetString(string key, string text)
    {
        return "\u0010" + key + "\u0012" + text + "\u0011";
    }
    public static string SetStrings(string key, string[] mass)
    {
        StringBuilder sb = new StringBuilder("\u0010" + key);
        foreach (string item in mass)
        {
            sb.Append("\u0012" + item);
        }
        sb.Append("\u0011");
        return sb.ToString();
    }
}
public struct Alias
{
    public const string Separator = "\u0012";
    public const string Connect = "\u0030";
    public const string Score = "\u0031";
    public const string TimeSinceStart = "\u0032";
    public const string SphereMass = "\u0033";
    public const string GlobalSeed = "\u0034";
    public const string Sphere = "\u0035";
    public const string SphereSolo = "\u0036";
    public const string Pop = "\u0037";
    public const string Ping = "\u0038";
}
