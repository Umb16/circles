﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

    public static Menu Instance;

    [SerializeField]
    InputField serverPort;
    [SerializeField]
    InputField spectatePort;
    [SerializeField]
    InputField serverAddress;

    Action Stop = () => { };

    public void StartGame()
    {
        Server server = new Server(int.Parse(serverPort.text));
        server.Start();
        CircleGenerator.Instance.StartGame();
        gameObject.SetActive(false);

        Stop = () =>
        {
            server.Stop();
        };
    }
    public void StartSpectate()
    {
        Client3.Instance.Connect(serverAddress.text, int.Parse(spectatePort.text));
        gameObject.SetActive(false);
        Stop = () =>
        {
            Client3.Instance.Stop();
        };
    }

    void OnEnable()
    {
        Stop();
        if(CircleGenerator.Instance!=null)
        CircleGenerator.Instance.Clear();
        CircleGenerator.Status = CircleGenerator.GameStatus.Menu;
    }
    // Use this for initialization
    void Start () {

        Instance = this;
    }
}
