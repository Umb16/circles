﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class CircleGenerator : MonoBehaviour
{
    public enum GameStatus
    {
        Menu,
        Play,
        Spectate
    }

    static GameStatus status = GameStatus.Menu;

    public static CircleGenerator Instance;

    [HideInInspector]
    public int GlobalSeed = 0;

    Transform thisTransform;
    [HideInInspector]
    public Vector2 WorldScreenSize;

    [SerializeField]
    GameObject CirclePrefab;
    [SerializeField]
    Text ScoreText;
    [SerializeField]
    Text TimeText;

    int score = 0;

    readonly float baseDelay = 1;

    float timeCounter = 0;

    int oldScore = 0;

    public int Level = 0;

    float startTime = 0;

    public List<Circle> CirclesList = new List<Circle>();
    public Dictionary<int, Circle> CirclesDict = new Dictionary<int, Circle>();


    float Delay
    {
        get { return baseDelay / (Level * .5f + 1); }
    }

    public static GameStatus Status
    {
        get
        {
            return status;
        }

        set
        {

            status = value;
        }
    }

    public void AddScore(int value)
    {
        score += value;

        ScoreText.text = "SCORE: " + score;

        Server.Instance.SendToAll(PushString.SetString(Alias.Score, score.ToString()));
    }

    public void Clear()
    {
        score = 0;
        oldScore = 0;
        Level = 0;
        TimeText.text = "TIME: 00:00:000";
        ScoreText.text = "SCORE: 0";
        TextureGenerator.Instance.DictinaryOfCircleImages.Clear();
        for (int i = 0; i < CirclesList.Count; i++)
        {
            if (CirclesList[i] != null)
            {
                Destroy(CirclesList[i].gameObject);
            }
        }
        CirclesList.Clear();
        CirclesDict.Clear();
    }

    void Start()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
        Instance = this;

        thisTransform = transform;

        AssetBundle bundle = AssetBundle.LoadFromFile(Application.streamingAssetsPath + "/bundle");
        CirclePrefab = bundle.LoadAsset<GameObject>("Sphere");

        WorldScreenSize.y = Camera.main.orthographicSize * 2;
        WorldScreenSize.x = (float)Screen.width / Screen.height * WorldScreenSize.y;



    }

    public void StartGame()
    {
        GlobalSeed = DateTime.Now.Millisecond;
        TextureGenerator.Instance.GenerateMaterials(GlobalSeed + Level, Level);
        startTime = Time.timeSinceLevelLoad;
        Status = GameStatus.Play;
    }

    public void StartSpectate()
    {
        Status = GameStatus.Spectate;
    }

    public void ClientSetTime(string time)
    {
        startTime = Time.timeSinceLevelLoad - float.Parse(time);
    }

    public void ClientSetScore(string score)
    {

        ScoreText.text = "SCORE: " + score;
    }

    public string GetGameInfoToSend()
    {
        string[] circleMass = new string[CirclesList.Count];
        for (int i = 0; i < CirclesList.Count; i++)
        {
            circleMass[i] = CirclesList[i].GetInfoToSend(Alias.Sphere);
        }

        return PushString.SetString(Alias.TimeSinceStart, (Time.timeSinceLevelLoad - startTime).ToString()) +
            PushString.SetString(Alias.Score, score.ToString()) +
            PushString.SetStrings(Alias.SphereMass, circleMass);
    }

    // Update is called once per frame
    void Update()
    {
        if (Status == GameStatus.Play)
        {
            CheckScoreToNextLevel();
            CheckCircleSpawn();
        }
        if (Status == GameStatus.Play || Status == GameStatus.Spectate)
            SetTimerText();
    }

    private void CheckCircleSpawn()
    {
        timeCounter += Time.deltaTime;
        if (timeCounter > Delay)
        {
            timeCounter -= Delay;
            CreateCircle(DateTime.Now.Millisecond);

        }
    }

    public void CreateCircleClient(int seed, int level, int id, Vector2 position)
    {
        if (CirclesDict.ContainsKey(id))
            return;
        if (TextureGenerator.Instance.GetMaterialsSet(level) == null)
        {
            TextureGenerator.Instance.GenerateMaterials(GlobalSeed + level, level);
        }

        GameObject tempCircle = Instantiate(CirclePrefab);
        tempCircle.transform.parent = thisTransform;
        Circle circleScript = tempCircle.GetComponent<Circle>();
        CirclesList.Add(circleScript);
        tempCircle.GetComponent<Circle>().Set(seed, TextureGenerator.Instance.GetMaterialsSet(level));
        tempCircle.transform.position = position;

        CirclesDict.Add(id, circleScript);
    }

    private void CreateCircle(int seed)
    {
        GameObject tempCircle = Instantiate(CirclePrefab);
        tempCircle.transform.parent = thisTransform;
        Circle circleScript = tempCircle.GetComponent<Circle>();
        CirclesList.Add(circleScript);
        tempCircle.GetComponent<Circle>().Set(seed, TextureGenerator.Instance.GetMaterialsSet(Level));
    }



    private void CheckScoreToNextLevel()
    {
        if (oldScore <= score - 1000)
        {
            oldScore = score;
            Level++;
            TextureGenerator.Instance.GenerateMaterials(GlobalSeed + Level, Level);
        }
    }

    private void SetTimerText()
    {
        float time = Time.timeSinceLevelLoad - startTime;
        TimeText.text = "TIME: " +
            (Mathf.FloorToInt(time / 60) % 60).ToString("00") + ":" +
            (Mathf.FloorToInt(time) % 60).ToString("00") + ":" +
            (Mathf.FloorToInt(time * 1000) % 1000).ToString("000");
    }
}
