﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class NetworkMessageQueue
{
    static Queue<string> messagesQueue = new Queue<string>();

    public static Action<string> Receiver;

    public static void AddMessage(string message)
    {
        messagesQueue.Enqueue(message);
    }

    public static void Update()
    {
        if (Receiver != null)
            if (messagesQueue.Count > 0)
                Receiver(messagesQueue.Dequeue());
    }
}
