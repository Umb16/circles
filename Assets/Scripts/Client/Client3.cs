﻿using System;
using UnityEngine;
using System.Net.Sockets;
using System.Text;
using System.Threading;

public class Client3 : MonoBehaviour
{

    public static Client3 Instance;

    public static TcpClient tcpClient;
    public static string LiveThreeDots = "";
    float liveThreeDotsTimer = 0;
    Thread thread;


    float pingTime = 0;
    public float ping = 0;

    // bool once = false;

    void Awake()
    {
        Instance = this;
        //DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        NetworkMessageQueue.Receiver = (text) =>
        {
            //Debug.Log(text);
            PushString pushString = new PushString(text);
            if (pushString.ContainKey(Alias.TimeSinceStart))
            {
                CircleGenerator.Instance.ClientSetTime(pushString.GetString(Alias.TimeSinceStart));
                CircleGenerator.Status = CircleGenerator.GameStatus.Spectate;
            }
            if (pushString.ContainKey(Alias.Score))
            {
                CircleGenerator.Instance.ClientSetScore(pushString.GetString(Alias.Score));
            }
            if (pushString.ContainKey(Alias.GlobalSeed))
            {
                CircleGenerator.Instance.GlobalSeed = int.Parse(pushString.GetString(Alias.GlobalSeed));
            }
            if (pushString.ContainKey(Alias.SphereMass))
            {
                string[] circles = pushString.GetStrings(Alias.SphereMass);
                for (int i = 0; i < circles.Length; i++)
                {
                    PushString ps = new PushString(circles[i]);
                    string[] circleParams = ps.GetStrings(Alias.Sphere);
                    if (circleParams.Length == 5)
                    {

                        CircleGenerator.Instance.CreateCircleClient(
                            int.Parse(circleParams[0]),
                            int.Parse(circleParams[1]),
                            int.Parse(circleParams[2]),
                            new Vector2(float.Parse(circleParams[3]), float.Parse(circleParams[4])));
                    }

                }
            }
            if (pushString.ContainKey(Alias.SphereSolo))
            {
                string[] circleParams = pushString.GetStrings(Alias.SphereSolo);
                if (circleParams.Length == 5)
                {
                   // print("SphereSolo");
                    CircleGenerator.Instance.CreateCircleClient(
                        int.Parse(circleParams[0]),
                        int.Parse(circleParams[1]),
                        int.Parse(circleParams[2]),
                        new Vector2(float.Parse(circleParams[3]), float.Parse(circleParams[4])));
                }
            }
            if (pushString.ContainKey(Alias.Pop))
            {
                int id = int.Parse(pushString.GetString(Alias.Pop));
                Circle temp;
                if (CircleGenerator.Instance.CirclesDict.TryGetValue(id, out temp))
                    temp.ClientPop();
            }
            if (pushString.ContainTag(Alias.Ping))
            {
                ping = 0;
            }
        };
    }

    public void StartSpectate(string IP, int port)
    {
        if (Connect(IP, port))
        {

        }
        else
        {
            Stop();
        }
    }

    public bool Connect(string IP, int port)
    {

        if (tcpClient == null || !tcpClient.Connected)
        {
            try
            {
                tcpClient = new TcpClient();
                tcpClient.Connect(IP, port);
            }
            catch (System.Exception ex)
            {
                Debug.Log(ex.Message);
                return false;
            }
            thread = new Thread(Reader);
            thread.Start();
        }
        return true;
    }

    public void Reader()
    {
        while (tcpClient != null && tcpClient.Connected)
        {
            string tempString = "";
            for (;;)
            {
                byte[] buf = new byte[1024];
                tcpClient.Client.Receive(buf, 0, buf.Length, SocketFlags.None);
                tempString += Encoding.UTF8.GetString(buf);
                if (Encoding.UTF8.GetString(buf).Contains("\u0011\0"))
                    break;
            }
            NetworkMessageQueue.AddMessage(tempString);
        }
    }

    public void Writer(string text)
    {
        if (tcpClient != null && tcpClient.Connected)
        {
            byte[] send = Encoding.UTF8.GetBytes(text + "\0");
            tcpClient.Client.Send(send);
            // Debug.Log("Send: " + text);
        }

    }

    void Update()
    {
        if (liveThreeDotsTimer < 1)
        {
            liveThreeDotsTimer += Time.deltaTime;
        }
        else
        {
            liveThreeDotsTimer = 0;
            if (LiveThreeDots.Length < 3)
                LiveThreeDots += ".";
            else
                LiveThreeDots = "";

        }

        if (tcpClient != null && tcpClient.Connected)
        {
            ping += Time.deltaTime;
            if (pingTime < 1)
            {
                pingTime += Time.deltaTime;
            }
            else
            {
                pingTime = 0;
                Writer(PushString.SetTag(Alias.Ping));
            }
        }
        NetworkMessageQueue.Update();

        if (tcpClient != null && !tcpClient.Connected||ping>6)
        {
            Stop();
        }

    }

    public void Stop()
    {
        try
        {

            tcpClient.Close();
            tcpClient = null;
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex.Message);
        }
        ping = 0;
        Menu.Instance.gameObject.SetActive(true);
    }

    void OnApplicationQuit()
    {
        Stop();
    }
}