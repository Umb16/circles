﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Circle : MonoBehaviour
{
    [SerializeField]
    Renderer renderer;

    [SerializeField]
    GameObject particleSysForLost;
    [SerializeField]
    GameObject particleSysForPop;

    public int Id = 0;
    static int idCounter = 0;

    float speed = 1;
    int score = 1;
    int seed = 0;

    TextureGenerator.LevelMaterialsSet matSet;

    Transform thisTransform;

    // Use this for initialization
    void Awake()
    {
        Id = idCounter++;
        thisTransform = transform;
    }

    public void Set(int seed, TextureGenerator.LevelMaterialsSet matSet)
    {
        this.seed = seed;
        System.Random rnd = new System.Random(seed);
        int type = rnd.Next(1, 5);
        score = (5 - type) * 30;
        float size = (type + 1) * .2f;
        speed = (2 - size) * .9f + matSet.Level * .2f;
        thisTransform.eulerAngles = new Vector3(rnd.Next(0, 360), rnd.Next(0, 360), rnd.Next(0, 360));
        thisTransform.position = new Vector2(Random.Range(-(CircleGenerator.Instance.WorldScreenSize.x - size), CircleGenerator.Instance.WorldScreenSize.x - size), CircleGenerator.Instance.WorldScreenSize.y - size) * .5f;
        renderer.material = matSet.Materials[type - 1];
        thisTransform.localScale = Vector3.one * size;
        renderer.material.color = matSet.Colour;
        matSet.ReferenceCount++;
        this.matSet = matSet;
        if (CircleGenerator.Status == CircleGenerator.GameStatus.Play)
            Server.Instance.SendToAll(GetInfoToSend(Alias.SphereSolo));
    }

    public string GetInfoToSend(string alias)
    {
        return PushString.SetStrings(alias, new string[]
            { seed.ToString(),
            matSet.Level.ToString(),
            Id.ToString(),
            thisTransform.position.x.ToString(),
            thisTransform.position.y.ToString()});

    }


    void CreateParticleSystem(GameObject particleSystemPrefab)
    {
        GameObject tempPatricleSys = Instantiate(particleSystemPrefab, thisTransform.position, Quaternion.identity);
        tempPatricleSys.transform.localScale = thisTransform.localScale;
        tempPatricleSys.GetComponent<ParticleSystemRenderer>().material.color = renderer.material.color;
        Destroy(tempPatricleSys, 3);
    }

    void OnDestroy()
    {
        matSet.ReferenceCount--;
        if (CircleGenerator.Instance.CirclesList.Contains(this))
        {
            CircleGenerator.Instance.CirclesList.Remove(this);
        }
        if (CircleGenerator.Instance.CirclesDict.ContainsKey(Id))
        {
            CircleGenerator.Instance.CirclesDict.Remove(Id);
        }
    }


    public void ClientPop()
    {
        Destroy(gameObject);
        CreateParticleSystem(particleSysForPop);
    }
    // Update is called once per frame
    void Update()
    {
        thisTransform.position += Vector3.down * Time.deltaTime * speed;
        if (thisTransform.position.y < (-CircleGenerator.Instance.WorldScreenSize.y + thisTransform.localScale.x) * .5f)
        {
            Destroy(gameObject);
            CreateParticleSystem(particleSysForLost);
        }

        if (Input.GetMouseButtonDown(0)&&CircleGenerator.Status == CircleGenerator.GameStatus.Play)
        {
            Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (Vector2.Distance(worldPoint, thisTransform.position) < thisTransform.localScale.x * .5f)
            {
                Destroy(gameObject);
                CreateParticleSystem(particleSysForPop);
                CircleGenerator.Instance.AddScore(score);
                if (CircleGenerator.Status == CircleGenerator.GameStatus.Play)
                    Server.Instance.SendToAll(PushString.SetString(Alias.Pop,Id.ToString()));
            }
        }
    }
}
