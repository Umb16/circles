﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

class Server
{
    Socket serverSocket;
    int port;
    public static Server Instance;

    public Server(int port)
    {
        this.port = port;
    }

    private void SetupServerSocket()
    {
        IPEndPoint endndpoint = new IPEndPoint(IPAddress.Any, port);

        serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        serverSocket.Bind(endndpoint);
        serverSocket.Listen(10);
    }

    private class ConnectionInfo
    {
        public Socket Socket;
        public byte[] Buffer;
        public bool AllOk;
    }

    private List<ConnectionInfo> connections = new List<ConnectionInfo>();

    public void Start()
    {
        Instance = this;
        SetupServerSocket();
        serverSocket.BeginAccept(AcceptCallback, serverSocket);

        // Thread thread = new Thread(CheckConnections);
    }

    public void Stop()
    {
        for (int i = 0; i < connections.Count; i++)
        {
            CloseConnection(connections[i]);
        }
        serverSocket.Close();
    }

    private void AcceptCallback(IAsyncResult result)
    {
        ConnectionInfo connection = new ConnectionInfo();
        try
        {
            Socket socket = (Socket)result.AsyncState;
            connection.Socket = socket.EndAccept(result);
            connection.Buffer = new byte[255];
            lock (connections)
                connections.Add(connection);
            connection.Socket.BeginReceive(connection.Buffer,
                0, connection.Buffer.Length, SocketFlags.None,
                ReceiveCallback,
                connection);
            serverSocket.BeginAccept(AcceptCallback, result.AsyncState);
        }
        catch (SocketException exc)
        {
            CloseConnection(connection);
            /*Logger.Warning("Socket exception: " +
                exc.SocketErrorCode);*/
        }
        catch (Exception exc)
        {
            CloseConnection(connection);
            // Logger.Warning("Exception: " + exc);
        }
    }


    private void ReceiveCallback(IAsyncResult result)
    {
        ConnectionInfo connection = (ConnectionInfo)result.AsyncState;
        try
        {
            int bytesRead = connection.Socket.EndReceive(result);
            if (bytesRead != 0)
            {
                PushString msg = new PushString(connection.Buffer);
                if (msg.ContainTag(Alias.Ping))
                {
                    connection.Socket.Send(Encoding.UTF8.GetBytes(PushString.SetTag(Alias.Ping)));
                }
                if (!connection.AllOk)
                {
                    connection.AllOk = true;
                    connection.Socket.Send(Encoding.UTF8.GetBytes(PushString.SetString(Alias.GlobalSeed, CircleGenerator.Instance.GlobalSeed.ToString())));
                    connection.Socket.Send(Encoding.UTF8.GetBytes(CircleGenerator.Instance.GetGameInfoToSend()));
                }


                //}
                connection.Socket.BeginReceive(
                    connection.Buffer, 0,
                    connection.Buffer.Length, SocketFlags.None,
                    ReceiveCallback,
                    connection);
            }
            else CloseConnection(connection);
        }
        catch (SocketException exc)
        {
            CloseConnection(connection);
            /* Logger.Warning("Socket exception: " +
                 exc.SocketErrorCode);*/
        }
        catch (Exception exc)
        {
            CloseConnection(connection);
            // Logger.Warning("Exception: " + exc);
        }
    }

    public void SendToAll(string text)
    {

        for (int i = 0; i < connections.Count; i++)
        {
            if (connections[i].AllOk)
            {
                var socket = connections[i].Socket;
                socket.Send(Encoding.UTF8.GetBytes(text));
            }
        }

    }

    private void CloseConnection(ConnectionInfo connection)
    {
        connection.Socket.Close();
        lock (connections)
            connections.Remove(connection);
    }

}
