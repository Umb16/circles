﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureGenerator
{
    Material baseMaterial;

    readonly int[] sizes = { 32, 64, 128, 256 };


    public Dictionary<int, LevelMaterialsSet> DictinaryOfCircleImages = new Dictionary<int, LevelMaterialsSet>();

    public class LevelMaterialsSet
    {
        int referenceCount;
        public int Level;
        public Material[] Materials;
        public Color Colour;

        public int ReferenceCount
        {
            get
            {
                return referenceCount;
            }

            set
            {
                referenceCount = value;
                if (referenceCount <= 0&& Level< CircleGenerator.Instance.Level)
                    Release();
            }
        }

        public void Release()
        {
            Instance.DictinaryOfCircleImages.Remove(Level);
            Resources.UnloadUnusedAssets();
        }
    }

    public class TextureCircle
    {
        public float X;
        public float Y;
        public Color Colour;
        public float Size;
    }
    static TextureGenerator instance;
    public static TextureGenerator Instance
    {
        get
        {
            if (instance == null) instance = new TextureGenerator();
            return instance;
        }
    }

    public LevelMaterialsSet GetMaterialsSet(int level)
    {
        if (DictinaryOfCircleImages.ContainsKey(level))
            return DictinaryOfCircleImages[level];
        else
            return null;
    }
    public void GenerateMaterials(int seed, int level)
    {
        if (baseMaterial == null)
            baseMaterial = new Material(Resources.Load<Material>("BaseMat"));
        System.Random rnd = new System.Random(seed);
        Color baseColor = new Color((float)rnd.NextDouble() * .5f + .5f, (float)rnd.NextDouble() * .5f + .5f, (float)rnd.NextDouble() * .5f + .5f);
        Material[] materials = new Material[4];
        for (int i = 0; i < materials.Length; i++)
        {
            materials[i] = new Material(baseMaterial);
            int size = sizes[i];
            Texture2D newTexture = new Texture2D(size, size, TextureFormat.ARGB32, false);
            Color[] texturePixels = new Color[size * size];

            int iterations = rnd.Next(4, 8);
            TextureCircle[] circles = new TextureCircle[iterations];
            for (int j = 0; j < iterations; j++)
            {
                circles[j] = new TextureCircle
                {
                    Colour = new Color((float)rnd.NextDouble(), (float)rnd.NextDouble(), (float)rnd.NextDouble()),
                    Size = (float)rnd.NextDouble() / 2 + .1f,
                    X = (float)rnd.NextDouble(),
                    Y = (float)rnd.NextDouble()
                };
            }

            for (int j = 0; j < texturePixels.Length; j++)
            {
                float x = j % size / (float)size;
                float y = j / size / (float)size;
                int persistance = 0;
                texturePixels[j] = baseColor;
                for (int n = 0; n < circles.Length; n++)
                {
                    TextureCircle circle = circles[n];
                    bool square = n % 3 == 0;
                    if (Vector2.Distance(new Vector2(x, y), new Vector2(circle.X, circle.Y)) < circle.Size && !square || square && x > circle.X - circle.Size / 2 && x < circle.X + circle.Size / 2 && y > circle.Y - circle.Size / 2 && y < circle.Y + circle.Size / 2)
                    {
                        if (n % 2 == 0)
                            persistance++;
                        else
                            persistance--;
                        if (rnd.Next(0, 8) == 0)
                            continue;
                        texturePixels[j] = circle.Colour;
                    }
                }
                if (persistance < 0)
                {
                    texturePixels[j] = baseColor;
                }

            }
            newTexture.SetPixels(texturePixels);
            newTexture.Apply();
            materials[i].mainTexture = newTexture;
        }
        DictinaryOfCircleImages.Add(level, new LevelMaterialsSet { Level = level, Materials = materials, Colour = baseColor });
    }
}
