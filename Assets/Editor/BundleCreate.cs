﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BundleCreate : MonoBehaviour {

    // Use this for initialization
    [MenuItem("Assets/Create bundle")]
    static void Create() {
        BuildPipeline.BuildAssetBundles(Application.streamingAssetsPath, BuildAssetBundleOptions.ChunkBasedCompression, EditorUserBuildSettings.activeBuildTarget);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
